# -*- coding: utf-8 -*-
import scrapy


class ItepRnSpider(scrapy.Spider):
    name = 'itep.rn'
    allowed_domains = ['www2.itep.rn.gov.br/sigep/public/listagem/obitos']
    start_urls = ['http://www2.itep.rn.gov.br/sigep/public/listagem/obitos']

    XPATH_TABLE_HEADER = '/html/body/div/div[2]/section/div/div[2]/table[%d]/thead/tr/th/text()'
    XPATH_TABLE_BODY = '/html/body/div/div[2]/section/div/div[2]/table[%d]/tbody/tr'

    XPATTH_TABLE_POSITION = {
        "header": XPATH_TABLE_HEADER,
        "body": XPATH_TABLE_BODY
    }

    TABLE_YESTERDAY = 1
    TABLE_TODAY = 2

    def _get_table_xpath(self, table, position):
        return self.XPATTH_TABLE_POSITION[position] % table

    def _parse_table(self, response, table):
        headers = response.xpath(self._get_table_xpath(table, 'header'))
        headers = [header.extract() for header in headers]

        lines = response.xpath(self._get_table_xpath(table, 'body'))
        for line in lines:
            obj = {}
            for index, column in enumerate(line.css('td')):
                c = column.css('small::text').extract_first()
                if not c:
                    obj[headers[index]] = None
                    continue
                obj[headers[index]] = c.strip()
            yield obj
        
    def parse(self, response):
        for line in self._parse_table(response, self.TABLE_YESTERDAY):
            yield line
        for line in self._parse_table(response, self.TABLE_TODAY):
            yield line